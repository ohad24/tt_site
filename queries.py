
load_user_sql = """
	SELECT u.id, u.classid, u.username, u.agencyid, a.agencyname,
			u.name, u.lastname, u.email,
			u.teamgroupid, tg.teamgroupname, c.userclassname
	FROM tt_site01.users u,
	tt_site01.teamsgroups tg,
	tt_site01.agencies a,
	tt_site01.refuserclasses c
	WHERE 1=1
	AND u.agencyid = a.id
	AND u.teamgroupid = tg.id
	AND u.classid = c.id
	AND u.id = %(user_id)s"""

login_user_sql = """
	SELECT a.id, a.classid, b.agencyname, a.teamgroupid 
	FROM tt_site01.users a
	LEFT JOIN tt_site01.agencies b ON a.agencyid = b.id
	WHERE a.agencyid = %(agencyid)s
	AND a.username = %(username)s
	AND a.passkey = %(passkey)s"""

get_user_future_shifts = """
	SELECT t.shiftid, t.teamgroupname, t.shiftdescription,
	t.fromdate,	t.todate, t.mandatoryemploeeysnum, t.optionalemploeeysnum,
	COALESCE(b.approvedby,0) approvedby, COALESCE(aa.count_, 0) count_,
	CASE 
		WHEN b.userid = %(uid)s AND b.approvedby = 0 THEN 'wait'
		WHEN b.userid = %(uid)s AND b.approvedby !=0 THEN 'approve'
		WHEN t.mandatoryemploeeysnum + t.optionalemploeeysnum <=  aa.count_ THEN 'full'
		ELSE 'open'
		END AS status_,
	userlist.ul
	FROM tt_site01.vusershift t
	LEFT JOIN tt_site01.shiftemprel b on t.shiftid = b.shiftid and b.userid = %(uid)s
	LEFT JOIN (SELECT a.shiftid,count(*) count_ FROM tt_site01.shiftemprel a
					GROUP BY a.shiftid) aa ON aa.shiftid = t.shiftid
	LEFT JOIN (SELECT r.shiftid, STRING_AGG(DISTINCT username::character varying ,', ' ORDER BY username) AS ul
				FROM tt_site01.shiftemprel r
				JOIN tt_site01.users u ON r.userid = u.id
				WHERE r.approvedby != 0
				group by r.shiftid) userlist ON t.shiftid = userlist.shiftid
	WHERE 1=1
	AND t.teamgroupid = %(teamgroupid)s
	AND t.todate > now() + (1 || 'hour')::INTERVAL
	AND t.approvemanagername IS NOT NULL --APPROVE SHIFT - NEED TO FIX QUERY
	ORDER BY t.fromdate"""

user_shifts_rel_ins_sql = """
	INSERT INTO tt_site01.shiftemprel (shiftid, userid, createdate, createby, approvedby, approveddate, modifiedby)
	VALUES (%(shiftid)s, %(userid)s, now(), %(createby)s, %(approvedby)s, %(approveddate)s, 0)"""

get_user_shifts_by_managerid = """
	SELECT t.userid, t.shiftid, t.teamgroupname, t.teamgroupid, t.fromdate, t.todate, t.mandatoryemploeeysnum,
		t.optionalemploeeysnum, t.approvedby, t.approveddate, t.shiftdescription, t.count_ AS count_total,
		t.count_approved AS count_approved, t.count_ - t.count_approved AS count_waiting
	FROM tt_site01.vmanageshifts t
	WHERE 1=1
	AND t.todate > now() + (1 || 'hour')::INTERVAL
	AND t.userid = %(userid)s
	ORDER BY t.teamgroupid, t.fromdate"""

get_usernames_by_shiftid = """
	SELECT t.userid, t.shiftid, t.username, t.createdate, t.approveddate, t.approvedby
	FROM tt_site01.vgetusersbyshiftid t
	where 1=1
	and t.shiftid = %(shiftid)s
	order by t.UserName """

shift_emp_rel_upd = """
	UPDATE tt_site01.shiftemprel t 
	SET approvedby = (SELECT CASE WHEN approvedby = '0' THEN %(mnguserid)s ELSE 0 END AS aa
						FROM tt_site01.shiftemprel WHERE shiftid = %(shiftid)s AND userid = %(userid)s),
	approveddate = now()
	WHERE shiftid = %(shiftid)s AND userid = %(userid)s"""

# need to update to get_users_count_to_shift
count_reg_usr_per_shift_sql = """
	SELECT COALESCE(count(*),0) count_ FROM tt_site01.shiftemprel a
	WHERE a.shiftid = %(shiftid)s"""

check_approved_user_by_shift_sql = """
	SELECT CASE WHEN approvedby != '0' THEN 1 ELSE 0 END AS emp_cur_status,
	t.approvedby,
	t.approveddate
	FROM tt_site01.shiftemprel t
	WHERE t.shiftid = %(shiftid)s AND t.userid = %(userid)s"""

teams_list_by_managerid_sql = """
	SELECT t.teamgroupid, l.teamgroupname FROM tt_site01.teamsmanagersrel t, tt_site01.teamsgroups l
	where t.teamgroupid = l.id
	and t.userid = %(userid)s"""

add_shift_sql = """
	INSERT INTO tt_site01.shifts (agencyid, teamgroupid, fromdate, todate, shiftdescription, mandatoryemploeeysnum, optionalemploeeysnum,
	approvedby, approveddate, createdate, createby)
	VALUES
	(%(agencyid)s, %(teamgroupid)s, %(fromdate)s, %(todate)s, %(shiftdescription)s, %(mandatoryemploeeysnum)s, %(optionalemploeeysnum)s, 
	%(approvedby)s, %(approveddate)s, %(createdate)s, %(createby)s)"""

remove_emp_from_shift_sql = """
	DELETE FROM tt_site01.shiftemprel
	where shiftid = %(shiftid)s and userid = %(empid)s"""

usernames_list_per_shift = """
	SELECT STRING_AGG(DISTINCT username::character varying ,', ' ORDER BY username) FROM tt_site01.shiftemprel r
	JOIN tt_site01.users u ON r.userid = u.id
	WHERE r.shiftid = %(shiftid)s
	and r.userid != %(userid)s"""

avaiable_users_to_shift = """
	SELECT u.id, u.username FROM tt_site01.users u
	WHERE u.id not IN (SELECT r.userid FROM tt_site01.shiftemprel r
						WHERE r.shiftid = %(shiftid)s)
	AND u.teamgroupid in (SELECT s.teamgroupid FROM tt_site01.shifts s
							where s.id = %(shiftid)s)"""

get_username_by_id = """
	SELECT t.username FROM tt_site01.users t
	WHERE id = %(userid)s """


get_users_count_to_shift = """
	SELECT COALESCE(t.count_,0) - COALESCE(t.count_approved,0) AS count_waiting, COALESCE(t.count_approved,0) AS count_approved
	FROM tt_site01.vmanageshifts t
	WHERE 1=1
	AND t.shiftid = %(shiftid)s"""

delete_shift = """
	DELETE FROM tt_site01.shiftemprel
	WHERE shiftid = %(shiftid)s;
	-- need to delete from restrections
	DELETE FROM tt_site01.shifts
	WHERE id = %(shiftid)s;"""

approve_shift = """
	UPDATE tt_site01.shifts SET approvedby = %(userid)s, approveddate = now()
	WHERE id = %(shiftid)s"""

insert_logs = """
	INSERT INTO tt_site01.Logs (requestmethod, statuscode, routename, codeplace, functionname,
		remote_addr, requestdata, sessiondata, createdate, createby)
		VALUES (%(requestmethod)s, %(statuscode)s, %(routename)s, %(codeplace)s, %(functionname)s, 
			%(remote_addr)s, %(requestdata)s, %(sessiondata)s, now(), %(createby)s)"""

get_user_presonal_detail = """
	SELECT t.id, t.username, t.teamgroupid, tg.teamgroupname, t.name, t.lastname, t.email,
			c.userclassname, a.agencyname, t.isblock
	FROM tt_site01.users t
	LEFT JOIN tt_site01.teamsgroups tg ON t.teamgroupid = tg.id
	LEFT JOIN tt_site01.refuserclasses c ON t.classid = c.id
	LEFT JOIN tt_site01.agencies a ON t.agencyid  = a.id
	WHERE t.id = %(empid)s"""

update_user_presonal_detail = """
	INSERT INTO tt_site01.users_hist
	SELECT t.id, t.username, t.agencyid, t.classid, t.teamgroupid, 
		t."name", t.lastname, t.email, t.passkey, t.isauth,
		t.isblock, t.lastlogin, now(), %(userid)s FROM tt_site01.users t
		WHERE t.id = %(userid)s;
	UPDATE tt_site01.users 
	SET email = %(email)s
	WHERE id = %(userid)s;"""

manage_employees_accordion = """
	SELECT tmrel.teamgroupid, tg.teamgroupname, t_emp_count.emp_count
	FROM tt_site01.teamsmanagersrel tmrel, tt_site01.teamsgroups tg
	LEFT JOIN (SELECT u.teamgroupid, count(*) AS emp_count
				FROM tt_site01.users u
				WHERE u.classid = 5
				GROUP BY u.teamgroupid) t_emp_count ON tg.id = t_emp_count.teamgroupid
	WHERE 1=1
	AND tmrel.teamgroupid = tg.id
	AND tmrel.userid = %(userid)s"""

username_list_manage_emp_accordion = """
	SELECT u.id, u.username, u.name, u.lastname, u.email, u.isblock FROM tt_site01.users u
	WHERE u.classid = 5 --only emps
	AND u.teamgroupid = %(teamgroupid)s
	ORDER BY u.id"""

update_user_detail_by_mng = """
	INSERT INTO tt_site01.users_hist
	SELECT t.id, t.username, t.agencyid, t.classid, t.teamgroupid, 
		t."name", t.lastname, t.email, t.passkey, t.isauth,
		t.isblock, t.lastlogin, now(), %(userid)s FROM tt_site01.users t
		WHERE t.id = %(empid)s;
	UPDATE tt_site01.users 
	SET name = %(empname)s, lastname = %(lastname)s, email = %(email)s, isblock=%(isblock)s
	WHERE id = %(empid)s;"""

update_emp_last_login = """
	UPDATE tt_site01.users
	SET lastlogin = now()
	WHERE id = %(userid)s"""

insert_user_by_manager = """
	INSERT INTO tt_site01.users (username, agencyid, classid, teamgroupid, name, lastname,
								email, passkey, isauth, isblock, createdate, createby)
	VALUES (%(username)s, %(agencyid)s, %(classid)s, %(teamgroupid)s,
			%(name)s, %(lastname)s, %(email)s, %(passkey)s,
			%(isauth)s, %(isblock)s, now(), %(mngid)s)"""

#for users and managers
get_shift_hist = """
	SELECT t.shiftid, t.teamgroupid, t.teamgroupname, t.fromdate, t.todate, t.shiftdescription,
	t.mandatoryemploeeysnum + t.optionalemploeeysnum AS total_emp_req, COALESCE(aa.count_, 0) AS emp_worked
	FROM tt_site01.vusershift t
	LEFT JOIN (SELECT a.shiftid,COUNT(*) count_ FROM tt_site01.shiftemprel a
				GROUP BY a.shiftid) aa ON aa.shiftid = t.shiftid
	WHERE t.todate < now() + (1 || ' hour')::INTERVAL
	AND CASE WHEN (SELECT u.classid FROM tt_site01.users u WHERE u.id = %(userid)s) = 5 /*emp*/ THEN
				t.teamgroupid in (SELECT u.teamgroupid FROM tt_site01.users u WHERE u.id = %(userid)s)
			WHEN (SELECT u.classid FROM tt_site01.users u WHERE u.id = %(userid)s) = 4 /*managers*/ THEN
				t.teamgroupid in (SELECT tmr.teamgroupid FROM tt_site01.teamsmanagersrel tmr
									WHERE tmr.userid = (SELECT u.id FROM tt_site01.users u WHERE u.id = %(userid)s))
			END
	ORDER BY t.fromdate DESC"""