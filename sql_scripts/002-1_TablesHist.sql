-- SET CONSTRAINTS ALL DEFERRED;

DROP TABLE IF EXISTS tt_site01.Users_Hist;

CREATE TABLE tt_site01.Users_Hist (
  Id SERIAL,
  UserName VARCHAR(45) NOT NULL,
  AgencyId integer NULL,
  ClassId integer NOT NULL,
  TeamGroupId integer NULL,
  Name VARCHAR(45) NULL,
  LastName VARCHAR(45) NULL,
  Email VARCHAR(45) NOT NULL,
  PassKey VARCHAR(45) NULL,
  IsAuth integer NULL,
  IsBlock integer NULL,
  LastLogin timestamp NULL,
  UpdateDate timestamp NOT NULL,
  UpdateBy integer NOT NULL
);