CREATE OR REPLACE FUNCTION tt_site01.spUserLogin (v_AgencyId INT, 
v_UserName VARCHAR(45), 
v_PassKey VARCHAR(45) )
RETURNS refcursor AS $$
DECLARE
ref refcursor;     
BEGIN
OPEN ref FOR SELECT a.Id, a.ClassId, b.AgencyName, a.TeamGroupId FROM tt_site01.Users a
left join tt_site01.Agencies b on a.AgencyId = b.Id
WHERE a.AgencyId = v_AgencyId AND a.UserName = v_UserName AND a.PassKey = v_PassKey;
RETURN ref;
END;
$$ LANGUAGE plpgsql;