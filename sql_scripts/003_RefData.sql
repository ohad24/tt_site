INSERT INTO tt_site01.RefUserClasses (UserClassName, CreateDate) VALUES ('SYSTEM', NOW());
INSERT INTO tt_site01.RefUserClasses (UserClassName, CreateDate) VALUES ('sysAdmin', NOW());
INSERT INTO tt_site01.RefUserClasses (UserClassName, CreateDate) VALUES ('Agency Admin', NOW());
INSERT INTO tt_site01.RefUserClasses (UserClassName, CreateDate) VALUES ('Team Manager', NOW());
INSERT INTO tt_site01.RefUserClasses (UserClassName, CreateDate) VALUES ('Employee', NOW());


INSERT INTO tt_site01.RefGroupTypes (GroupTypeName, CreateDate) VALUES ('sysAdmins', now());
INSERT INTO tt_site01.RefGroupTypes (GroupTypeName, CreateDate) VALUES ('Agency Admins', now());
INSERT INTO tt_site01.RefGroupTypes (GroupTypeName, CreateDate) VALUES ('Managers', now());
INSERT INTO tt_site01.RefGroupTypes (GroupTypeName, CreateDate) VALUES ('Emoloyees', now());


-- SELECT * FROM tt_site01.Agencies;
-- delete from tt_site01.Agencies;

INSERT INTO tt_site01.Agencies (AgencyName, Description, CreateDate) VALUES ('sysAdmin Agency', 'sysAdmins Dummy', now());
INSERT INTO tt_site01.Agencies (AgencyName, Description, CreateDate) VALUES ('Dummy Agency 1', 'Description 1', now());
INSERT INTO tt_site01.Agencies (AgencyName, Description, CreateDate) VALUES ('Dummy Agency 2', 'Description 2', now());


-- SELECT * FROM tt_site01.TeamsGroups;
-- delete from tt_site01.TeamsGroups;

INSERT INTO tt_site01.TeamsGroups (TeamGroupName, AgencyId, GroupTypeId, CreateDate) VALUES ('Agc1Admin1', 2, 2, Now());
INSERT INTO tt_site01.TeamsGroups (TeamGroupName, AgencyId, GroupTypeId, CreateDate) VALUES ('Agc1Mnagers1', 2, 3, Now());
INSERT INTO tt_site01.TeamsGroups (TeamGroupName, AgencyId, GroupTypeId, CreateDate) VALUES ('Agc1employees1', 2, 4, Now());
INSERT INTO tt_site01.TeamsGroups (TeamGroupName, AgencyId, GroupTypeId, CreateDate) VALUES ('Agc1employees2', 2, 4, Now());



INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('ohad24', 1, 2, null, 'Ohad', 'Mizrachi', 'o@o.com', '1234', 1, 0, now());
    
INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('AgenciyAdm1', 2, 3, 1, 'Agnc1', 'Adm1', 'Agnc1@Adm1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('AgenciyAdm2', 2, 3, 1, 'Agnc2', 'Adm2', 'Agnc2@agc1.com', '1234', 1, 0, now());
    
INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Mng1', 2, 4, 2, 'Manger1', 'Mng1', 'Manger1@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Mng2', 2, 4, 2, 'Manger2', 'Mng2', 'Manger2@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp1', 2, 5, 3, 'Employee1', 'Emp1', 'Employee1@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp2', 2, 5, 3, 'Employee2', 'Emp2', 'Employee2@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp3', 2, 5, 3, 'Employee3', 'Emp3', 'Employee3@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp4', 2, 5, 3, 'Employee4', 'Emp4', 'Employee4@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp5', 2, 5, 3, 'Employee5', 'Emp5', 'Employee5@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp6', 2, 5, 3, 'Employee6', 'Emp6', 'Employee6@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp7', 2, 5, 3, 'Employee7', 'Emp7', 'Employee7@agc1.com', '1234', 1, 0, now());
    
INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp8', 2, 5, 3, 'Employee8', 'Emp8', 'Employee8@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp9', 2, 5, 3, 'Employee9', 'Emp9', 'Employee8@agc9.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp10', 2, 5, 4, 'Employee10', 'Emp10', 'Employee10@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp11', 2, 5, 4, 'Employee11', 'Emp11', 'Employee11@agc1.com', '1234', 1, 0, now());

INSERT INTO tt_site01.Users (UserName, AgencyId, ClassId, TeamGroupId, Name, LastName, Email, PassKey, IsAuth, IsBlock, CreateDate) 
	VALUES ('Emp12', 2, 5, 4, 'Employee12', 'Emp12', 'Employee12@agc1.com', '1234', 1, 0, now());
	

INSERT INTO tt_site01.TeamsManagersRel (UserId, TeamGroupId) VALUES (4, 3);
INSERT INTO tt_site01.TeamsManagersRel (UserId, TeamGroupId) VALUES (4, 4);

INSERT INTO tt_site01.TeamsManagersRel (UserId, TeamGroupId) VALUES (5, 3);
	
	
DO $$ 
<<first_block>>
DECLARE
  DATE_ timestamp;
  time_ time;
  dt timestamp;
  dtto timestamp;
  sss TEXT;
BEGIN 
   DATE_ := current_date + integer '5';
   time_ := time '09:00';
   dt := DATE_ + time_;
   sss := '3';
   -- dtto := dt + interval :sss hour;
   -- dtto := dt + (sss || ' hour')::INTERVAL;
   -- RAISE NOTICE '% - %s', DATE_, dt;
   
   INSERT INTO tt_site01.Shifts (AgencyId, TeamGroupId, FromDate, ToDate, ShiftDescription, MandatoryEmploeeysNum, OptionalEmploeeysNum, ApprovedBy, ApprovedDate) 
   VALUES 
	('2', '3', 
	dt,
	dt + (sss || ' hour')::INTERVAL,
	'Regular Shift 1', '3', '0', '4', now());
   
   dt := dt + (sss || ' hour')::INTERVAL;
   
   INSERT INTO tt_site01.Shifts (AgencyId, TeamGroupId, FromDate, ToDate, ShiftDescription, MandatoryEmploeeysNum, OptionalEmploeeysNum, ApprovedBy, ApprovedDate)
   VALUES 
	('2', '3',
	dt,
	dt + (sss || ' hour')::INTERVAL,
	'Regular Shift 2', '3', '0', '4', now());

	dt := dt + (sss || ' hour')::INTERVAL;
   
   INSERT INTO tt_site01.Shifts (AgencyId, TeamGroupId, FromDate, ToDate, ShiftDescription, MandatoryEmploeeysNum, OptionalEmploeeysNum, ApprovedBy, ApprovedDate)
   VALUES
	('2', '3',
	dt,
	dt + (sss || ' hour')::INTERVAL,
	'Regular Shift 3', '3', '0', '4', now());

   dt := dt + (sss || ' hour')::INTERVAL;

   INSERT INTO tt_site01.Shifts (AgencyId, TeamGroupId, FromDate, ToDate, ShiftDescription, MandatoryEmploeeysNum, OptionalEmploeeysNum, ApprovedBy, ApprovedDate)
   VALUES
	('2', '3',
	dt,
	dt + (sss || ' hour')::INTERVAL,
	'Regular Shift 4', '3', '0', '4', now());

	
	
	dt := DATE_ + time_;
	dt := dt + (sss || ' hour')::INTERVAL;
	
	INSERT INTO tt_site01.Shifts (AgencyId, TeamGroupId, FromDate, ToDate, ShiftDescription, MandatoryEmploeeysNum, OptionalEmploeeysNum, ApprovedBy, ApprovedDate)
	VALUES
	 ('3', '4',
	 dt,
	 dt + (sss || ' hour')::INTERVAL,
	 'Regular Shift 1', '3', '0', '4', now());
	
	dt := dt + (sss || ' hour')::INTERVAL;

	INSERT INTO tt_site01.Shifts (AgencyId, TeamGroupId, FromDate, ToDate, ShiftDescription, MandatoryEmploeeysNum, OptionalEmploeeysNum, ApprovedBy, ApprovedDate)
	VALUES
	 ('3', '4', 
	 dt,
	 dt + (sss || ' hour')::INTERVAL,
	 'Regular Shift 1', '3', '0', '4', now());


END first_block $$;
