
SET CONSTRAINTS ALL DEFERRED;

DROP TABLE IF EXISTS tt_site01.Logs;
DROP TABLE IF EXISTS tt_site01.ShiftsEmpRestrictions;
DROP TABLE IF EXISTS tt_site01.ShiftEmpRel;
DROP TABLE IF EXISTS tt_site01.Shifts;
DROP TABLE IF EXISTS tt_site01.TeamsManagersRel;
DROP TABLE IF EXISTS tt_site01.Users;
DROP TABLE IF EXISTS tt_site01.TeamsGroups;
DROP TABLE IF EXISTS tt_site01.Agencies;
DROP TABLE IF EXISTS tt_site01.RefGroupTypes;
DROP TABLE IF EXISTS tt_site01.RefUserClasses;


/* TEST TABLES */

DROP TABLE IF EXISTS tt_site01.TEST01;
CREATE TABLE IF NOT EXISTS tt_site01.TEST01(
	Id SERIAL,
	CreateDate timestamp,
	CreateBy VARCHAR(45)
);

/* END TEST TABLES */
  
CREATE TABLE IF NOT EXISTS tt_site01.RefUserClasses(
	Id SERIAL,
	UserClassName VARCHAR(45),
	CreateDate timestamp NOT NULL,
	CreateBy integer,
	PRIMARY KEY (Id),
	UNIQUE (UserClassName)
);

CREATE TABLE IF NOT EXISTS tt_site01.RefGroupTypes (
  Id SERIAL,
  GroupTypeName VARCHAR(45) NOT NULL,
  CreateDate timestamp NOT NULL,
  CreateBy integer NULL,
  PRIMARY KEY (Id),
  UNIQUE (GroupTypeName)
);


CREATE TABLE IF NOT EXISTS tt_site01.Agencies (
  Id SERIAL,
  AgencyName VARCHAR(45) NOT NULL,
  Description VARCHAR(2000) NULL,
  CreateDate timestamp NULL,
  CreateBy integer NULL,
  PRIMARY KEY (Id),
  UNIQUE (AgencyName));
 

CREATE TABLE tt_site01.TeamsGroups (
  Id SERIAL,
  TeamGroupName VARCHAR(45) NOT NULL,
  AgencyId integer NULL,
  GroupTypeId integer NULL,
  CreateDate timestamp NULL,
  CreateBy integer NULL,
  PRIMARY KEY (Id),
  
    FOREIGN KEY (AgencyId)
    REFERENCES tt_site01.Agencies (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
	
    FOREIGN KEY (GroupTypeId)
    REFERENCES tt_site01.RefGroupTypes (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE tt_site01.Users (
  Id SERIAL,
  UserName VARCHAR(45) NOT NULL,
  AgencyId integer NULL,
  ClassId integer NOT NULL,
  TeamGroupId integer NULL,
  Name VARCHAR(45) NULL,
  LastName VARCHAR(45) NULL,
  Email VARCHAR(45) NOT NULL,
  PassKey VARCHAR(45) NULL,
  IsAuth integer NULL,
  IsBlock integer NULL,
  LastLogin timestamp NULL,
  CreateDate timestamp NULL,
  CreateBy integer NULL,
  PRIMARY KEY (Id),
  UNIQUE (Email),
  
    FOREIGN KEY (AgencyId)
    REFERENCES tt_site01.Agencies (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 
    FOREIGN KEY (ClassId)
    REFERENCES tt_site01.RefUserClasses (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
	
    FOREIGN KEY (TeamGroupId)
    REFERENCES tt_site01.RefGroupTypes (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

DROP TABLE IF EXISTS tt_site01.TeamsManagersrel;
CREATE TABLE tt_site01.TeamsManagersrel (
  Id SERIAL,
  UserId integer NOT NULL,
  TeamGroupId integer,
  CreateDate timestamp,
  CreateBy integer,
  PRIMARY KEY (Id),

    FOREIGN KEY (UserId)
    REFERENCES tt_site01.Users (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

    FOREIGN KEY (TeamGroupId)
    REFERENCES tt_site01.TeamsGroups (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


CREATE TABLE tt_site01.Shifts (
  Id SERIAL,
  AgencyId integer NOT NULL,
  TeamGroupId integer NOT NULL,
  FromDate timestamp NOT NULL,
  ToDate timestamp,
  ShiftDescription VARCHAR(800),
  MandatoryEmploeeysNum integer,
  OptionalEmploeeysNum integer,
  ApprovedBy integer,
  ApprovedDate timestamp,
  CreateDate timestamp,
  CreateBy integer,
  PRIMARY KEY (Id),

    FOREIGN KEY (AgencyId)
    REFERENCES tt_site01.Agencies (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

    FOREIGN KEY (TeamGroupId)
    REFERENCES tt_site01.TeamsGroups (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE tt_site01.ShiftEmpRel (
  Id SERIAL,
  ShiftId integer NOT NULL,
  UserId integer NOT NULL,
  CreateDate timestamp,
  CreateBy integer,
  ApprovedDate timestamp,
  ApprovedBy integer,
  ModifiedDate timestamp,
  ModifiedBy integer,
  PRIMARY KEY (Id),
  UNIQUE (ShiftId, UserId),
  
    FOREIGN KEY (ShiftId)
    REFERENCES tt_site01.Shifts (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  
    FOREIGN KEY (UserId)
    REFERENCES tt_site01.Users (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
	
CREATE TABLE tt_site01.ShiftsEmpRestrictions (
  Id SERIAL,
  UserId integer NOT NULL,
  RestrictDateFrom timestamp NOT NULL,
  RestrictDateTo timestamp,
  ReasonDescription VARCHAR(2000) NOT NULL,
  ApprovedDate timestamp,
  ApprovedBy integer,
  CreateDate timestamp,
  CreateBy integer,
  PRIMARY KEY (Id),
  
    FOREIGN KEY (UserId)
    REFERENCES tt_site01.Users (Id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE tt_site01.Logs (
  Id SERIAL,
  RequestMethod VARCHAR(25),
  StatusCode integer,
  RouteName VARCHAR(200),
  CodePlace VARCHAR(200),
  FunctionName VARCHAR(200),
  Remote_Addr INET,
  RequestData jsonb,
  SessionData jsonb,
  AdditionalDataJson jsonb,
  AdditionalDataStr VARCHAR(8000),
  CreateDate timestamp,
  CreateBy integer
);

CREATE UNIQUE INDEX idx_logs_int_dt ON tt_site01.logs (createdate DESC);