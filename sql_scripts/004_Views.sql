create or replace view tt_site01.VUsers1 as
select u.Id, u.UserName,uc.UserClassName, a.AgencyName, tg.TeamGroupName, rgt.GroupTypeName, u.Name, u.Email, u.CreateDate from tt_site01.Users u
left join tt_site01.RefUserClasses uc on u.ClassId = uc.Id
left join tt_site01.Agencies a on u.AgencyId = a.Id
left join tt_site01.TeamsGroups tg on u.TeamGroupId = tg.Id
left join tt_site01.RefGroupTypes rgt on tg.GroupTypeId = rgt.Id;


create or replace view tt_site01.VUserShift as
SELECT a.Id as "shiftid",a.TeamGroupId, b.TeamGroupName, a.FromDate, a.ToDate, a.MandatoryEmploeeysNum,
a.OptionalEmploeeysNum, c.UserName as ApproveManagerName, a.shiftdescription
FROM tt_site01.Shifts a
left join tt_site01.TeamsGroups b on a.TeamGroupId = b.Id
left join tt_site01.Users c on a.ApprovedBy = c.Id
where 1=1
and ApprovedBy is not null;

CREATE OR REPLACE VIEW tt_site01.vmanageshifts AS
SELECT tmr.userid, s.id AS shiftid, s.teamgroupid, s.agencyid, s.fromdate, s.todate, s.mandatoryemploeeysnum,
	s.optionalemploeeysnum, s.approvedby, s.approveddate, 
	s.shiftdescription, tg.teamgroupname, COALESCE(c.count_,0) AS count_,
	COALESCE(c.count_approved,0) AS count_approved
FROM tt_site01.teamsgroups tg,
	tt_site01.TeamsManagersRel tmr,
	tt_site01.shifts s
LEFT JOIN (SELECT t.shiftid,
			COUNT(*) AS count_,
			SUM(CASE WHEN t.approvedby != 0 THEN 1 ELSE 0 END) AS count_approved
			FROM tt_site01.shiftemprel t
			GROUP BY t.shiftid) c ON c.shiftid = s.id
WHERE 1=1
AND s.teamgroupid = tmr.teamgroupid
AND s.teamgroupid = tg.id
ORDER BY fromdate;

-- select t.UserId, t.UserName, t.ShiftId, t.TeamGroupId, t.FromDate, t.ToDate, 
-- t.MandatoryEmploeeysNum, t.OptionalEmploeeysNum, t.ApprovedBy, t.ApprovedDate
-- from VManageShifts t
-- where 1=1


create or replace view tt_site01.VGetUsersByShiftId as
SELECT b.Id as UserId, t.ShiftId, b.UserName, t.CreateDate, t.ApprovedDate, t.ApprovedBy
FROM tt_site01.ShiftEmpRel t,
tt_site01.Users b
WHERE t.UserId = b.Id
ORDER BY t.ShiftId;
