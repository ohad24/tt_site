$(document).ready(function(){

	function create_emp_tbl (v_teamgroupid) {
		// console.log(v_teamgroupid)
		$.getJSON($SCRIPT_ROOT + '/_edit_emp_tbl', {
			teamgroupid: v_teamgroupid
		},
		function(data) {
			$.each( $.makeArray(data), function( i, val ) {
				// console.log(val)
				if (val.isblock == 0) {
					var row = $('<tr>')
				} else {
					var row = $('<tr class="table-danger">')
				}
				row.append($('<th scope="row">' + val.id + '</th>'));
				row.append($('<td id="userid-' + val.id + '"><div data-toggle="modal" data-target="#emp_detailModel" id="empid-' + val.id + '" val="'+ val.id +'" href="#" class="u_list"><a href="#" class="text-primary">' + val.username + '</a></div></td>'));
				row.append($('<td>' + val.name + '</td>'));
				row.append($('<td>' + val.lastname + '</td>'));
				row.append($('<td>' + val.email + '</td>'));
				$("#t_body-" + v_teamgroupid).append(row);
				$('#userid-' + val.id).on( "click", ".u_list", showuser );
			});
		});
	};
	
	
	
	$( ".c_teamgroupid" ).each(function( index ) {
		var v_teamgroupid = $(this).attr("value");
		create_emp_tbl(v_teamgroupid);
	});
	
	var $edf = $('.emp_det_field_upd');
	var old_emd_name;
	var old_emd_lastname;
	var old_emd_email;
	var old_emd_isblock;
	var g_mng_upd_empid;
	var popemp_teamgroupid
	
	function update_emp_det_by_manager() {
		var v_em_empname = $("#ed_empname").val();
		var v_em_lastname = $("#ed_lastname").val();
		var v_em_email = $("#ed_email").val();
		
		if ($('#ed_isblock').is(':checked')) {
			var v_em_isblock = 1
		} else {
			var v_em_isblock = 0
		};
		// console.log(v_em_isblock);
		if ( v_em_empname != old_emd_name || 
				v_em_lastname != old_emd_lastname ||
				v_em_email != old_emd_email ||
				v_em_isblock != old_emd_isblock) {
				
				$.post($SCRIPT_ROOT + '/_update_user_detail_by_mng', {
					empid : g_mng_upd_empid,
					empname : v_em_empname,
					lastname : v_em_lastname,
					email : v_em_email,
					isblock :v_em_isblock
				});
				// $.getJSON($SCRIPT_ROOT + '/_update_user_detail_by_mng', );
			$edf.attr('readonly', true).attr('contenteditable','false').removeAttr('style');
			$('#ed_empname').val(v_em_empname);
			$('#ed_lastname').val(v_em_lastname);
			$('#ed_email').val(v_em_email);
			$('#ed_isblock').val(v_em_isblock);
			$("#empopenedit").html("Edit");
			$("#ed_isblock").attr("disabled", true);
			
			$.notify('Employee details update successfully', 'success');
			
			//var v_teamgroupid = popemp_teamgroupid;
			$('#emp_detailModel').modal('hide')
			$("#t_body-" + popemp_teamgroupid).empty();
			create_emp_tbl(popemp_teamgroupid);
		};
			
	};
	
	$('#submit_update_emp_det').on("click", function () {
		if ($edf.attr('readonly') == undefined) {
			update_emp_det_by_manager()
		}	
	});

	$('#empopenedit').click(function() {
		var old_empname = $("#ed_empname").val();
		var old_lastname = $("#ed_lastname").val();
		var old_email = $("#ed_email").val();
		
		if ($edf.attr('readonly')) {
			$edf.removeAttr('readonly')
			.attr('contenteditable','true')
			.css({"color":"green","border":"2px solid green"});
			$("#empopenedit").html("Cancel");
			$("#ed_isblock").removeAttr("disabled");
		} else {
			$edf.attr('readonly', true).attr('contenteditable','false').removeAttr('style');
			// $('#ud_email').val(old_ude_email);
			$("#empopenedit").html("Edit");
			$("#ed_isblock").attr("disabled", true);
			$("#ed_empname").val(old_empname);
			$("#ed_lastname").val(old_lastname);
			$("#ed_email").val(old_email);
		}
	});
	
	function showuser() {
		// console.log($(this));
		v_empid = $(this).attr('val');
		g_mng_upd_empid = v_empid;
		$.getJSON($SCRIPT_ROOT + '/_get_emp_detail', {
			empid: v_empid
		},
		function(data) {
			popemp_teamgroupid = data.teamgroupid;
			$("#ed_agencyname").val(data.agencyname);
			$("#ed_teamgroupname").val(data.teamgroupname);
			$("#ed_userclassname").val(data.userclassname);
			$("#ed_empname").val(data.name);
			$("#ed_lastname").val(data.lastname);
			$("#ed_email").val(data.email);
			// console.log(data.isblock)
			if (data.isblock == 1) {
				$("#ed_isblock").prop('checked', true);
				// console.log($("#ed_isblock").is(":checked"));
			} else {
				$("#ed_isblock").prop('checked', false);
				// console.log($("#ed_isblock").is(":checked"));
			};
			$("#ed_isblock").attr("disabled", true);
			
			$( "#emp_detailModelLabel").empty().append( "<span>Employees Details: </span>"+data.username );

		});
	};
	$(".u_list").on("click", showuser);
	
});