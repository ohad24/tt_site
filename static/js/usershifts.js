$(document).ready(function(){
	
	function applyShift() {
		var shiftid = $(this).val();
		$.getJSON($SCRIPT_ROOT + '/_apply_to_shift', {
			shiftid: shiftid // ,
			// empid: empid
		},
		function(data) {
			$("#btn"+shiftid).removeClass("btn btn-secondary")
			.removeClass("applyshift")
			.addClass("btn btn-info")
			.html("Waiting for approval") //NEED TO FIX TO GLOBAL VARS (PYTHON ?)
			$('#cnt'+shiftid).html(data.result);
			$("#btn"+shiftid).off();
		}
		);
	};
	
	$(".applyshift").on("click", applyShift);

	function cancelShift() {
		var shiftid = $(this).val();
		$.getJSON($SCRIPT_ROOT + '/_cancel_shift', {
			shiftid: shiftid
		},
		function(data) {
			$('#btn'+shiftid).removeClass("btn btn-info")
			.addClass("btn btn-secondary")
			.addClass("applyshift")
			.val(shiftid)
			.html("Applay");
			$('#btnc'+shiftid).hide();
			$('#cnt'+shiftid).html(data.result);
		});
		$('#card-' + shiftid).on( "click", "#btn" + shiftid, applyShift );
	};

	$(".cancelshift").on("click", cancelShift);

});
