$(document).ready(function(){
	
	var shifts_data
	var shift_data
	
	// refresh main table when model is closed
	$('#shift_editModel').on('hidden.bs.modal', function () {
		create_shifts_tbl()
	});
	
	function create_shifts_tbl () {
		$.getJSON($SCRIPT_ROOT + '/_get_future_shifts_by_mng', {},
		function(data) {
			// console.log(data)
			$('#t_body').empty();
			shifts_data = data
			$.each( $.makeArray(data), function( i, val ) {
				if (val.approvedby != 0) {
					var row = $('<tr>');
					if (val.count_approved >= val.mandatoryemploeeysnum) {
						var row = $('<tr class="table-success">');
					} else {
						var row = $('<tr>');
					}
				} else {
					var row = $('<tr class="table-danger">');
				}
				row.append($('<th scope="row">' + val.shiftid + '</th>'));
				row.append($('<td id="shiftid-' + val.shiftid + '"><div data-toggle="modal" data-target="#shift_editModel" id="shift-' + val.shiftid + '" val="'+ val.shiftid +'" href="#" class="s_list"><a href="#" class="text-primary">' + val.shiftdescription + '</a></div></td>'));
				row.append($('<td>' + val.teamgroupname + '</td>'));
				row.append($('<td>' + val.fromdate + '</td>'));
				row.append($('<td>' + val.todate + '</td>'));
				row.append($('<td>' + val.mandatoryemploeeysnum + '</td>'));
				row.append($('<td>' + val.optionalemploeeysnum + '</td>'));
				row.append($('<td>' + val.count_total + '</td>'));
				row.append($('<td>' + val.count_approved + '</td>'));
				row.append($('<td>' + val.count_waiting + '</td>'));
				$("#t_body").append(row);
				// $('#shiftid-' + val.shiftid).on("click", ".s_list", showshiftdetails);
				//$('#shiftid-' + val.shiftid).click({shiftid: val.shiftid}, showshiftdetails);
				$('#shiftid-' + val.shiftid).click( function () {
					showshiftdetails(val.shiftid);
				});
			});
		});
	};
	
	create_shifts_tbl()
	
	function approveShift() {
		var shiftid = shift_data.shiftid;
		$.post($SCRIPT_ROOT + '/_approve_shift', {shiftid: shiftid});
		$("#shift_approve_status").empty().addClass('approve').text('Approved').css('color', 'green');	
		$(".approve_btn").remove();
	};
	
	function showshiftdetails(shiftid) {
		v_shiftid = shiftid
		shift_data = shifts_data.find(function(element) {
		if (element.shiftid == v_shiftid) {
				return element;
			}
		});
		if (shift_data.approvedby == 0) {
			$("#shift_approve_status").empty().text('Not Approved').css('color', 'red');
			var approve_btn = $('<button/>',
			{
				text: 'Approve Shift',
				type: 'button',
				addClass: 'btn btn-success approve_btn',
				click: function () { approveShift()} 
			});
			$('.approve_btn').remove();
			$("#modal-footer_shift_emps").append(approve_btn);
			
		} else {
			$("#shift_approve_status").empty().addClass('approve').text('Approved').css('color', 'green');
		}
		$("#delte_shift_btn").val(v_shiftid);
		
		$.getJSON($SCRIPT_ROOT + '/_get_future_shift_det_by_mng', {shiftid : v_shiftid},
		function(r_shift_data) {
			$("#shift_detailModelLabel").empty().append("#" + shift_data.shiftid + " <span>" + shift_data.shiftdescription + "</span>" );
			
			// gen avaiable_emps_select
			$("#avaiable_emps_select").empty();
			$.each(r_shift_data['avaiable_users_forms_d'], function(key, value) {   
				$('#avaiable_emps_select')
					.append($("<option></option>")
					.attr("value", value[0])
					.text(value[1])); 
			});
			
			// gen emps table per shifts
			$("#t_body_s_det").empty();
			$.each( $.makeArray(r_shift_data['shift_emp_data']), function( i, val ) {
				var row = $('<tr>')
				row.append($('<th scope="row">' + val.userid + '</th>'));
				row.append($('<td>' + val.username + '</td>'));
				row.append($('<td id="approvedby_emp_cel-' + val.userid + '">' + val.approvedby + '</td>'));
				if (val.approvedby == 0) {
					row.append($('<td id="approveddate_emp_cel-' + val.userid + '"></td>'));
				} else {
					row.append($('<td id="approveddate_emp_cel-' + val.userid + '">' + val.approveddate + '</td>'));
				};
				if (val.approvedby != 0) {
					row.append($('<td id="approve_btn_emp_cel-' + val.userid + '"><a class="emp_row text-warning" empid=' + val.userid + ' href="#">DisApprove User</a></td>'));
				} else {
					row.append($('<td id="approve_btn_emp_cel-' + val.userid + '"><a class="emp_row text-success" empid=' + val.userid + ' href="#">Approve User</a></td>'));
				}
				// row.append($('<td id="userid-' + val.id + '"><div data-toggle="modal" data-target="#emp_detailModel" id="empid-' + val.id + '" val="'+ val.id +'" href="#" class="u_list"><a href="#" class="text-primary">' + val.username + '</a></div></td>'));
				row.append('<td id="remove_btn_emp_cel-' + val.userid + '"><a class="remove-emp text-danger" empid=' + val.userid + ' href="#">Remove User</a></td>');
				$("#t_body_s_det").append(row);
				
				$('#approve_btn_emp_cel-' + val.userid).on( "click", ".emp_row", approve_emp_to_shift );
				$('#remove_btn_emp_cel-' + val.userid).on( "click", ".remove-emp", remove_emp );
			});
			
		});
	};

	function add_emp_to_shifts () {
		v_empid = $("#avaiable_emps_select option:selected" ).val();
		if ( v_empid != undefined ) {
			$.post($SCRIPT_ROOT + '/_apply_to_shift_mng', {
				shiftid: shift_data.shiftid,
				empid: v_empid}
			);
			showshiftdetails(shift_data.shiftid);
		}
	}
	
	$("#addempbtn").click(add_emp_to_shifts);
	
	function remove_emp() {
		var empid = $(this).attr('empid');
		$.post($SCRIPT_ROOT + '/_cancel_shift', { shiftid: shift_data.shiftid, empid: empid });
		showshiftdetails(shift_data.shiftid);
	};
	
	$(".remove-emp").on( "click", remove_emp );
	
	function approve_emp_to_shift() {
		v_empid = $(this).attr('empid');
		v_shiftid = shift_data.shiftid;
		$a = $(this)
		$.post($SCRIPT_ROOT + '/_approve_user_to_shift', {
				shiftid: v_shiftid,
				empid: v_empid
			},
			function(data) {
				// console.log(data);
				if (data.emp_cur_status == '1') {
					$a.removeClass('text-success')
					.addClass('text-warning')
					.text('DisApprove User')
					$('#approvedby_emp_cel-' + v_empid).text(data.approvedby);
					$('#approveddate_emp_cel-' + v_empid).text(data.approveddate);
					}
					else { 
					$a.removeClass('text-warning')
					.addClass('text-success')
					.text('Approve User')
					$('#approvedby_emp_cel-' + v_empid).text(0);
					$('#approveddate_emp_cel-' + v_empid).empty()
				}
			},
			);
	}
	
	$(".emp_row").on("click", approve_emp_to_shift);
	
	
	function deleteShift() {
		var shiftid = $(this).val();
		
		var confirmAnswer = confirm("are you sure?");
		if (confirmAnswer == true) {
			$.post($SCRIPT_ROOT + '/_delete_shift', {shiftid: shiftid});
			$('#shift_editModel').modal('hide')
		}
	};
	$("#delte_shift_btn").on("click", deleteShift);
	
});