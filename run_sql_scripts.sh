#!/bin/bash

#psql -U the_user the_database <<OMG

psql -h $PGSERVER -p $PGPORT -d dev ohad <<OMG
BEGIN;

\i sql_scripts/001_Schemas.sql

\i sql_scripts/002_Tables.sql

\i sql_scripts/002-1_TablesHist.sql

\i sql_scripts/003_RefData.sql

\i sql_scripts/004_Views.sql
COMMIT;

\i sql_scripts/009_BackupData.sql

COMMIT;
OMG

#\i sql_scripts/005_Procedures.sql
