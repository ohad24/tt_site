import logging, __main__, os
from logging.handlers import RotatingFileHandler

if not os.path.exists(__main__.config['DEFAULT']['logdir']):
	os.makedirs(__main__.config['DEFAULT']['logdir'])

logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
# __main__.app.logger.setLevel(logging.DEBUG)

logger_level = eval(__main__.config.get(__main__.args.env, 'loglevel'))

logger = logging.getLogger('werkzeug')
logger.setLevel(logger_level)

w_fileHandler  = RotatingFileHandler(os.path.join(__main__.config['DEFAULT']['logdir'], 'werkzeug.log'), maxBytes=10000, backupCount=1)
w_fileHandler.setLevel(logger_level)
w_fileHandler.setFormatter(logFormatter)
logger.addHandler(w_fileHandler)
# __main__.app.logger.addHandler(w_fileHandler)
# 

if __main__.args.env in ['dev','test']:
	consoleHandler = logging.StreamHandler()
	consoleHandler.setLevel(logger_level)
	consoleHandler.setFormatter(logFormatter)
	logger.addHandler(consoleHandler)
	# __main__.app.logger.addHandler(consoleHandler)

f_log = logging.getLogger('flask')
f_fileHandler  = RotatingFileHandler(os.path.join(__main__.config['DEFAULT']['logdir'], 'flask.log'), maxBytes=10000)
# f_fileHandler = logging.StreamHandler()
f_fileHandler.setLevel(logger_level)
f_fileHandler.setFormatter(logFormatter)
f_log.setLevel(logger_level)
f_log.addHandler(f_fileHandler)
# __main__.app.logger.addHandler(f_fileHandler)

# app.logger.warning('hello')
# app.logger.debug('debug')
