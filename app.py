import psycopg2

from flask import Flask, render_template, redirect, url_for, request, session, jsonify, abort, app, g, flash, send_from_directory
# from flask import request_finished
# import werkzeug
from flask_bootstrap import Bootstrap
from flask_login import LoginManager, UserMixin, \
						login_required, login_user, logout_user, current_user
from flask_wtf import FlaskForm
import wtforms as wtf
from wtforms.validators import InputRequired, Email, Length, AnyOf, DataRequired

from flask_wtf.csrf import CSRFProtect

from wtforms_components import DateTimeField
from wtforms.fields.html5 import DateField, DateTimeField, EmailField
from werkzeug.datastructures import MultiDict

import os, datetime, time, pprint, ast, socket, configparser, argparse, logging, json
from functools import wraps
import git
git_ = git.Git(".") 

import psql_api
import queries

# https://stackoverflow.com/questions/14888799/disable-console-messages-in-flask-server
import logging

from OpenSSL import SSL
# context = SSL.Context(SSL.SSLv23_METHOD)
# context.use_privatekey_file('ssl.key')
# context.use_certificate_file('ssl.cert')
# log = logging.getLogger('werkzeug')
# log.setLevel(logging.ERROR)

# def handle_bad_request(e):
# 	return redirect(url_for('index')), 403

parser = argparse.ArgumentParser()
parser.add_argument('-env', '--env', help='can be dev/test/prod', choices=['dev', 'test', 'prod'], action="store", required=True)
args = parser.parse_args()

config = configparser.ConfigParser()
config.read('config.ini')

app = Flask(__name__)
app.secret_key = 'secret'
# import errors
Bootstrap(app)
login_manager = LoginManager()
login_manager.init_app(app)

# import errors

dtFmt = '%H:%M %d/%m/%y'
htmlDtFmt = '%Y-%m-%dT%H:%M'

# from logger import logger, f_log

conn=psycopg2.connect(
		database=config[args.env]['database'],
		user=config[args.env]['user'],
		host=config[args.env]['host'],
		password=config[args.env]['password'],
		port=config[args.env]['port']
		)

conn.autocommit = True
cur = conn.cursor()


def requires_roles(*roles):
	def wrapper(f):
		@wraps(f)
		def wrapped(*args, **kwargs):
			if current_user.cid > roles[0]:
				abort(403)
			return f(*args, **kwargs)
		return wrapped
	return wrapper


user_instances = set()
class User(UserMixin):
	def __init__(self, user_id):
		cur = conn.cursor()
		cur.execute(queries.load_user_sql, ({'user_id' : user_id}))
		sql_data = psql_api.PostgresAPI(cur).to_list()
		self.id = sql_data['id']
		self.cid = sql_data['classid']
		self.username = sql_data['username']
		self.agencyid = sql_data['agencyid']
		self.agencyname = sql_data['agencyname']
		self.teamgroupid = sql_data['teamgroupid']
		self.teamgroupname = sql_data['teamgroupname']
		self.userclassname = sql_data['userclassname']
		self.name = sql_data['name']
		self.lastname = sql_data['lastname']
		self.email = sql_data['email']
		user_instances.add(self.id)

class LoginForm(FlaskForm):
	sqlCmd = 'SELECT Id, AgencyName FROM tt_site01.Agencies'
	cur.execute(sqlCmd)
	agenciesList = cur.fetchall()
	agenciesList = [(str(x[0]), x[1]) for x in agenciesList]
	agency = wtf.SelectField('Agency', choices = agenciesList, default=2)
	username = wtf.StringField('Username', validators=[InputRequired()])
	password = wtf.PasswordField('Password', \
				validators=[InputRequired(), Length(min=2, max=10)])

class avaiable_users(FlaskForm):
	emp_to_selection = wtf.SelectField('Avaiable Users', coerce=int, default=('',''))

class UserShiftsAppForm():
	UStatus = { "no" : "No", 
				"yes" : "Yes", 
				"wait" : "Waiting for approval",
				"approve" : "Shift Approved",
				"open" : "Open",
				"full" : "Shift is full"}

class CreateShiftForm(FlaskForm):
	agencyname = wtf.StringField('Agency', render_kw={'readonly': True})
	t_list = wtf.SelectField('Team List')
	shift_name = wtf.StringField('Shift Name/Desc', validators=[InputRequired(), Length(min=5, max=80)])
	from_dt = wtf.DateTimeField('Start At', format=htmlDtFmt, render_kw={'type': 'datetime-local'}, \
								default=datetime.datetime.now(), validators=[InputRequired()])
	duration = wtf.SelectField('Duration', choices = [(str(i), str(i)) for i in range(10)], default = 8)
	mand = wtf.IntegerField('Mandatory Emploeeys', default = 1)
	opti = wtf.IntegerField('Optional Emploeeys', default = 0)
	approved = wtf.BooleanField('Approve Shift', default = False)

class CreateUserForm(FlaskForm):
	agencyname = wtf.StringField('Agency', render_kw={'readonly': True})
	t_list = wtf.SelectField('Team List')
	c_list = wtf.SelectField('User Class', coerce=int)
	username = wtf.StringField('Username', [InputRequired(), Length(min=5, max=20, message='Username must be more then 5 characters')])
	name = wtf.StringField('Name', validators=[InputRequired(), Length(min=5, max=20, message='Name must be more then 5 characters')])
	lastname = wtf.StringField('Lastname', validators=[InputRequired(), Length(min=5, max=20, message='Name must be more then 5 characters')])
	email = wtf.StringField('Email Address', [wtf.validators.DataRequired(), wtf.validators.Email("Email is not valid")])
	password = wtf.PasswordField('New Password', [Length(min=5, max=20, message='Passwords must be more then 5 characters'),
		wtf.validators.DataRequired(),
		wtf.validators.EqualTo('confirm', message='Passwords must match')
	])
	confirm = wtf.PasswordField('Repeat Password')
	isauth = wtf.BooleanField('Authenticate', default = True)
	isblock = wtf.BooleanField('Blocked', default = False)
	submit = wtf.SubmitField("Add User")

class EditUserForm(FlaskForm):
	agencyname = wtf.StringField('Agency')
	userclassname = wtf.StringField('User Class')
	teamgroupname = wtf.StringField('Team Group')
	empname = wtf.StringField('Name')
	lastname = wtf.StringField('Last Name')
	email = wtf.StringField('Email')
	isblock = wtf.BooleanField('Is Block')

@app.context_processor
def load_user_details_nav_form(): #no content
	usernavform = EditUserForm()
	return dict(usernavform=usernavform)

@app.before_request
def make_session_permanent():
	session.permanent = True
	app.permanent_session_lifetime = datetime.timedelta(minutes=120)

def db_log(stc = None, response = None, codeplace = 'app.py'):
	r_data = { "args" : request.args, "form" : request.form.to_dict(flat=False), "data" : str(request.data) }
	status_code = response.status_code if stc is None else stc
	cur = conn.cursor()
	cur.execute(queries.insert_logs,
		({'requestmethod' : request.method,'statuscode' : status_code, 'routename' : request.path, 
			'codeplace' : codeplace, 'functionname' : 'db_log', 'remote_addr' : request.remote_addr,
			'requestdata': json.dumps(r_data),'sessiondata' : json.dumps(dict(session)),
			 'createby' : current_user.id if current_user.is_authenticated == True else 0}))

@app.after_request
def log_data_after(response):
	db_log(response = response)
	return response

@app.route('/favicon.ico')
def favicon():
	return send_from_directory(os.path.join(app.root_path, 'static/img/'),
								'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/")
def index():
	return redirect(url_for('home'))

@app.route("/home")
def home():
	# logger.warning('hello')
	# logger.debug('debug')
	# f_log.warning('hello f_log')
	# f_log.debug('debug f_log')
	# app.logger.error('hello')
	# app.logger.debug('debug')
	loginfo = git_.log('--since=2.weeks','--pretty=tformat:%h|%ar|%ad|%s')

	l_git_last_commit = []
	for i in loginfo.split('\n'):
		l = i.split('|')
		l_git_last_commit.append(l)
	return render_template('index.html', l_git_last_commit = l_git_last_commit)

@app.route("/shift-hist", methods=["GET", "POST"])
@login_required
def shifthist():
	cur.execute(queries.get_shift_hist,
		({'userid' : current_user.id}))
	sql_data = psql_api.PostgresAPI(cur).to_list()
	for i, row in enumerate(sql_data):
		row['fromdate'] = row['fromdate'].strftime(dtFmt)
		row['todate'] = row['todate'].strftime(dtFmt)
		sql_data[i] = row
	return render_template('shifthist.html', shift_hist_data = sql_data)

@app.route("/shifts", methods=["GET", "POST"])
@login_required
def usershifts():
	cur = conn.cursor()
	cur.execute(queries.get_user_future_shifts,
		({'uid' : current_user.id, 'teamgroupid' : current_user.teamgroupid}))
	sql_data = psql_api.PostgresAPI(cur).to_list()
	for i, row in enumerate(sql_data):
		row['fromdate'] = row['fromdate'].strftime(dtFmt)
		row['todate'] = row['todate'].strftime(dtFmt)
		sql_data[i] = row
	return render_template('usershifts.html', UStatus = UserShiftsAppForm.UStatus, rowsd = sql_data)

@app.route("/create-users", methods=["GET", "POST"])
@login_required
@requires_roles(4)
def createusers():
	createuserform = CreateUserForm()
	cur = conn.cursor()
	cur.execute(queries.teams_list_by_managerid_sql,
		({'userid' : current_user.id}))
	team_list = cur.fetchall()
	team_list = [(str(x[0]), x[1]) for x in team_list]
	app.logger.debug(team_list)
	createuserform.t_list.choices = team_list
	createuserform.c_list.choices = [(5, "Employee")]
	createuserform.c_list.default = 1
	createuserform.agencyname.data = current_user.agencyname
	if request.method == 'POST':
		if createuserform.validate_on_submit():
			app.logger.debug(request.form)
			cur.execute(queries.insert_user_by_manager,
					({'username' : request.form['username'],
						'agencyid' : current_user.agencyid,
						'classid' : request.form['c_list'],
						'teamgroupid' : request.form['t_list'],
						'name' : request.form['name'],
						'lastname' : request.form['lastname'],
						'email' : request.form['email'],
						'passkey' : request.form['password'],
						'isauth' : 1 if 'isauth' in request.form.keys() else 0,
						'isblock' : 1 if 'isblock' in request.form.keys() else 0,
						'mngid' : current_user.id
						}))
			flash('User Created', 'success')
		for fieldName, errorMessages in createuserform.errors.items():
			for err in errorMessages:
				# app.logger.debug([fieldName, err])
				flash(err, 'error')
	return render_template('createusers.html', createuserform = createuserform)

@app.route("/create-shifts", methods=["GET", "POST"])
@login_required
@requires_roles(4)
def createshifts():
	cur = conn.cursor()
	cur.execute(queries.teams_list_by_managerid_sql,
		({'userid' : current_user.id}))
	team_list = cur.fetchall()
	team_list = [(str(x[0]), x[1]) for x in team_list]
	form = CreateShiftForm()
	form.t_list.choices = team_list
	form.agencyname.data = current_user.agencyname

	if form.validate_on_submit() and request.method == 'POST':
		app.logger.debug(request.form)
		from_dt = datetime.datetime.strptime(request.form['from_dt'], htmlDtFmt)
		approvedby, approveddate = (current_user.id, datetime.datetime.now()) if 'approved' in request.form else (0, None)
		cur.execute(queries.add_shift_sql, ({'agencyid' : current_user.agencyid, 'teamgroupid' : request.form['t_list'],
			'fromdate' : from_dt, 'todate' : from_dt + datetime.timedelta(hours=int(request.form['duration'])),
			'shiftdescription' : request.form['shift_name'],
			'mandatoryemploeeysnum' : request.form['mand'],
			'optionalemploeeysnum' : request.form['opti'],
			'approvedby' : approvedby,
			'approveddate' : approveddate,
			'createdate' : datetime.datetime.now(),
			'createby' : current_user.id} ) )
		flash('Shift has been created', 'success')
		return redirect(url_for('createshifts'))
	return render_template('createshifts.html', form=form)

@app.route("/mng-shifts", methods=["GET", "POST"])
@login_required
@requires_roles(4) #team managers
def mngshifts():
	return render_template('mngshifts.html')

# @app.route("/mng-shifts", methods=["GET", "POST"])
# @login_required
# @requires_roles(4) #team managers
# def mngshifts():
# 	cur = conn.cursor()
# 	cur.execute(queries.get_user_shifts_by_managerid, ({'userid' : current_user.id}))
# 	cur_data = psql_api.PostgresAPI(cur)
# 	if cur_data.data_size()[0] == 1:
# 		return render_template('mngshifts.html')

# 	avaiable_users_forms_d = {}
# 	sql_data = cur_data.to_list()
# 	for i, row in enumerate(sql_data):	#get all shifts
# 		row['fromdate'] = row['fromdate'].strftime(dtFmt)
# 		row['todate'] = row['todate'].strftime(dtFmt)

# 		cur.execute(queries.get_usernames_by_shiftid, ({'shiftid' : row['shiftid']}))
# 		UsersList = cur.fetchall()
# 		userslist = [list(r) for r in UsersList]
# 		row['userslist'] = userslist
		
# 		sql_data[i] = row

# 		cur.execute(queries.avaiable_users_to_shift, ({'userid' : current_user.id, 'shiftid' : row['shiftid']}))
# 		form_user_data = cur.fetchall()
# 		form_user_data = [(str(x[0]), x[1]) for x in form_user_data]
# 		form = avaiable_users()
# 		form.emp_to_selection.choices = form_user_data
# 		form.emp_to_selection.render_kw = {'id' : 'cid' + str(row['shiftid']), 'class' : 'form-control'}

# 		avaiable_users_forms_d[row['shiftid']] = form
		
# 	return render_template('mngshifts.html', ldrows = sql_data, UStatus = UserShiftsAppForm.UStatus, avaiable_users_forms_d = avaiable_users_forms_d)

@app.route("/manage-employees", methods=["GET", "POST"])
@login_required
@requires_roles(4)
def manageemployees():
	empdetails = EditUserForm()
	# empdetails.isblock(disabled=True)
	cur = conn.cursor()
	cur.execute(queries.manage_employees_accordion, ({'userid' : current_user.id}))
	manage_employees_accordion_data = psql_api.PostgresAPI(cur).to_list()
	# app.logger.debug(type(manage_employees_accordion_data))
	if type(manage_employees_accordion_data) is dict:
		manage_employees_accordion_data = [manage_employees_accordion_data]
	for i, teamgrouprow in enumerate(manage_employees_accordion_data):
		# app.logger.debug(teamgrouprow)
		cur.execute(queries.username_list_manage_emp_accordion, ({'teamgroupid' : teamgrouprow['teamgroupid']}))
		teamgrouprow['emp_list'] = psql_api.PostgresAPI(cur).to_dict('id')
		manage_employees_accordion_data[i] = teamgrouprow
	return render_template('manageemployees.html',
								manage_employees_accordion_data = manage_employees_accordion_data
								, empdetails = empdetails
								)

@app.route("/login", methods=["GET", "POST"])	
def login():
	cur = conn.cursor()
	form = LoginForm()
	if request.method == 'POST':
		agencyid = request.form['agency']
		username = request.form['username']
		password = request.form['password']
		cur.execute(queries.login_user_sql,
			({'agencyid' : agencyid, 'username' : username, 'passkey' : password}))
		sql_data = psql_api.PostgresAPI(cur).to_list()
		if len(sql_data) > 0:
			user = User(sql_data['id'])
			login_user(user)
			flash('Logged in successfully.', 'success')
			cur.execute(queries.update_emp_last_login, ({'userid' : current_user.id}))
			return redirect(url_for('index'))
		else:
			flash('Login faild.', 'error')
	return render_template('login.html', form=form)

@app.route("/logout")
@login_required
def logout():
	user_instances.remove(current_user.id)
	logout_user()
	return redirect(url_for('index'))

@login_manager.user_loader
def load_user(user_id):
	return User(user_id)

from os import path
extra_dirs = ['templates','static']
extra_files = extra_dirs[:]
for extra_dir in extra_dirs:
	for dirname, dirs, files in os.walk(extra_dir):
		for filename in files:
			filename = path.join(dirname, filename)
			if path.isfile(filename):
				extra_files.append(filename)

if __name__ == '__main__':
	import errors
	import js_funcs
	app.logger.setLevel(config[args.env]['loglevel'])
	context = ('ssl.cert', 'ssl.key')
	app.run(host='0.0.0.0', debug=config[args.env]['flask_debug'], threaded=True, port=5000, extra_files=extra_files, ssl_context=context)