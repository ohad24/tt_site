from __main__ import *

# multipage (used more then one page)

@app.route('/_update_user_presonal_detail', methods=["POST"]) #from navbar
@login_required
def update_user_detail():
	cur = conn.cursor()
	cur.execute(queries.update_user_presonal_detail, ({'email' : request.form['email'],
														'userid' : current_user.id}))
	return jsonify(None)

@app.route('/_cancel_shift', methods=["POST"]) #managers ignores output (should be the same for users)
@login_required
def remove_emp_from_shift():
	cur = conn.cursor()
	# shiftid = request.args.get('shiftid', 0, type=int)
	# userid = request.args.get('userid', current_user.id, type=int)
	cur.execute(queries.remove_emp_from_shift_sql,
		({'shiftid' : request.form['shiftid'], 'empid' : request.form['empid']}))
	# cur.execute(queries.count_reg_usr_per_shift_sql,
	# 	({'shiftid' : shiftid}))
	# sql_data = psql_api.PostgresAPI(cur).to_list()
	return jsonify(None)

# managers

@app.route('/_get_future_shifts_by_mng')
@login_required
def get_future_shifts_by_mng():
	cur = conn.cursor()
	cur.execute(queries.get_user_shifts_by_managerid, ({'userid' : current_user.id}))
	cur_data = psql_api.PostgresAPI(cur)
	if cur_data.data_size()[0] == 1:
		return render_template('mngshifts.html')

	avaiable_users_forms_d = {}
	sql_data = cur_data.to_list()
	for i, row in enumerate(sql_data):	#get all shifts
		row['fromdate'] = row['fromdate'].strftime(dtFmt)
		row['todate'] = row['todate'].strftime(dtFmt)
	return jsonify(sql_data)

@app.route('/_get_future_shift_det_by_mng')
@login_required
def get_future_shift_det_by_mng():
	cur = conn.cursor()
	shiftid = request.args.get('shiftid', 0, type=int)
	cur.execute(queries.get_usernames_by_shiftid, ({'shiftid' : shiftid}))
	emps_tbl_data = psql_api.PostgresAPI(cur).to_list()
	# app.logger.debug(sql_data)

	avaiable_users_forms_d = {}
	cur.execute(queries.avaiable_users_to_shift, ({'userid' : current_user.id, 'shiftid' : shiftid}))
	form_user_data = cur.fetchall()
	app.logger.debug(form_user_data)
	form_user_data = [(str(x[0]), x[1]) for x in form_user_data]
	form = avaiable_users()
	form.emp_to_selection.choices = form_user_data
	form.emp_to_selection.render_kw = {'id' : 'cid' + str(shiftid), 'class' : 'form-control'}

	avaiable_users_forms_d[shiftid] = form


	return jsonify({'shift_emp_data' : emps_tbl_data,
					'avaiable_users_forms_d' : form_user_data})

@app.route('/_update_user_detail_by_mng', methods=["POST"])
@login_required
def update_user_detail_by_mng():
	cur.execute(queries.update_user_detail_by_mng, ({'empid' : request.form['empid'],
													'empname' : request.form['empname'],
													'lastname' : request.form['lastname'],
													'email' : request.form['email'],
													'isblock' : request.form['isblock'],
													'userid' : current_user.id}))
	return jsonify(result=None)

@app.route("/_edit_emp_tbl")
def edit_emp_tbl():
	# empdetails = UserNavForm()
	teamgroupid = request.args.get('teamgroupid', 0, type=int)
	cur = conn.cursor()
	cur.execute(queries.username_list_manage_emp_accordion, ({'teamgroupid' : teamgroupid}))
	# emp_list = psql_api.PostgresAPI(cur).to_dict('id')
	emp_list = psql_api.PostgresAPI(cur).to_list()
	return jsonify(emp_list)

@app.route('/_get_emp_detail')
@login_required
def get_emp_detail():
	empid = request.args.get('empid', 0, type=int)
	cur = conn.cursor()
	cur.execute(queries.get_user_presonal_detail, ({'empid' : empid }))
	sql_data = psql_api.PostgresAPI(cur).to_list()
	return jsonify(sql_data)

@app.route('/_user_list_selection_mng')
@login_required
def user_list_selection_mng():
	cur = conn.cursor()
	shiftid = request.args.get('shiftid', 0, type=int)
	cur.execute(queries.avaiable_users_to_shift, ({'userid' : current_user.id, 'shiftid' : shiftid}))
	form_user_data = cur.fetchall()
	form_user_data = [(str(x[0]), x[1]) for x in form_user_data]
	return jsonify({'result' : form_user_data})

@app.route('/_apply_to_shift_mng', methods=["POST"])
@login_required
def apply_to_shift_mng():
	cur = conn.cursor()
	# shiftid = request.args.get('shiftid', 0, type=int)
	# empid = request.args.get('empid', 0, type=int)
	# approvedby = request.args.get('approvedby', 0, type=int)
	cur.execute(queries.user_shifts_rel_ins_sql,
		({'shiftid' : request.form['shiftid'], 'userid' : request.form['empid'],
			'approvedby' : current_user.id,
			'createby' : current_user.id,
			'approveddate' : datetime.datetime.now()
			}))
	# cur.execute(queries.get_username_by_id, ({'userid' : empid}))
	# v_username = cur.fetchone()
	return jsonify(None)

@app.route('/_delete_shift', methods=["POST"])
@login_required
def delete_shift():
	cur = conn.cursor()
	# shiftid = request.args.get('shiftid', 0, type=int)
	cur.execute(queries.delete_shift,({'shiftid' : request.form['shiftid']}))
	return jsonify(None)

@app.route('/_update_user_count')
@login_required
def update_user_count():
	time.sleep(0.05) #whait for the recoreds update
	shiftid = request.args.get('shiftid', 0, type=int)
	t_cur = conn.cursor()
	t_cur.execute(queries.get_users_count_to_shift, ({'shiftid' : shiftid}))
	sql_data = psql_api.PostgresAPI(t_cur).to_list()
	return jsonify(result=sql_data)

@app.route('/_approve_user_to_shift', methods=["POST"])
@login_required
def approve_user_to_shift():
	cur = conn.cursor()
	empid = request.form['empid']
	shiftid = request.form['shiftid']
	cur.execute(queries.shift_emp_rel_upd,
		({'mnguserid' : current_user.id, 'userid' : empid, 'shiftid' : shiftid})) #update table
	cur.execute(queries.check_approved_user_by_shift_sql,
		({'userid' : empid, 'shiftid' : shiftid})) #get current value (after update)
	sql_data = psql_api.PostgresAPI(cur).to_list()
	return jsonify(sql_data)

@app.route('/_approve_shift', methods=["POST"]) #from shift manage page
@login_required
def approve_shift():
	cur = conn.cursor()
	cur.execute(queries.approve_shift, ({'shiftid' : request.form['shiftid'], 'userid' : current_user.id}))
	return jsonify(None)

# employees

@app.route('/_apply_to_shift')
@login_required
def apply_to_shift():
	cur = conn.cursor()
	shiftid = request.args.get('shiftid', 0, type=int)
	#  empid = request.args.get('empid', 0, type=int)
	approvedby = 0
	cur.execute(queries.user_shifts_rel_ins_sql,
		({'shiftid' : shiftid, 'userid' : current_user.id,
			'approvedby' : approvedby,
			'createby' : current_user.id,
			'approveddate' : None
			}))
	cur.execute(queries.count_reg_usr_per_shift_sql,
		({'shiftid' : shiftid}))
	sql_data = psql_api.PostgresAPI(cur).to_list()
	return jsonify(result=sql_data['count_'])