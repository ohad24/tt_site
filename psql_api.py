#!/media/ohad/data1/py_projects/envs/Py3.6-Flask/bin/python

import psycopg2

class PostgresAPI():
	def __init__(self, cur):
		self.data = tuple(x for x in cur)
		self.desc = tuple(x for x in cur.description)
		# print(self.desc)

	def data_size(self):
		rows = len(self.data)
		cols = len(self.data[0]) if rows != 0 else 0
		return (rows, cols)

	def to_list(self):
		data_list = []
		for row in self.data:
			tmp = []
			for i, val in enumerate(row):
				tmp.append((self.desc[i][0], val))
			drow = dict(tmp)
			data_list.append(drow)
		if len(data_list) == 1:
			data_list = data_list[0]
		return data_list

	def to_dict(self, ikey=None):
		data_list = {}
		for i, row in enumerate(self.data):
			data_list[i] = {}
			for j, val in enumerate(row):
				tmp = data_list[i]
				tmp[self.desc[j][0]] = val
				data_list[i] = tmp
		if ikey:
			k2rm = []
			d_tmp = data_list.items()
			s = set(x[ikey] for x in data_list.values())
			if len(s) != len(data_list):
				raise Exception('Duplicate keys in column')
			for k, v in list(d_tmp):
				tmp = dict(v)
				del v[ikey]
				data_list[tmp[ikey]] = v
				del data_list[k]
		return data_list