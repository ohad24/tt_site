from __main__ import *

@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404

@app.errorhandler(401)
def unauthorized(e):
	flash('Please login.', 'info')
	db_log(stc = e.code, codeplace = 'errors.py')
	app.logger.debug('4011111')
	return redirect(url_for('login'))

@app.errorhandler(403)
def Forbidden(e):
	# app.logger.debug(dir(e))
	app.logger.debug('4033333')
	db_log(stc = e.code, codeplace = 'errors.py')
	flash('You are not authorize.', 'error')
	return redirect(url_for('home'))

